package server;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;  // Import the File class
import java.sql.*;
import java.util.StringTokenizer;

import rminterface.RMIInterface;

public class Server extends UnicastRemoteObject implements RMIInterface{

	private static final long serialVersionUID = 1L;

	protected Server() throws RemoteException {

			super();

	}

	// @Override
	public String helloTo(String name) throws RemoteException{
		//Consulta recebida
		String[] input = new String[20];
		System.err.println("-----------------//---------------");
		System.err.println("Consulta:"+ name);
		System.err.println("-----------------//---------------");

		/*time 1*/
		long tv1 = System.nanoTime();

		String delimeters = " :";

		StringTokenizer defaultTokenizer = new StringTokenizer(name, delimeters);

		//quebra a consulta em vários argumentos
		int i = 0;
		while (defaultTokenizer.hasMoreTokens()) {
			input[i] = defaultTokenizer.nextToken();
			i += 1;
		}

		//armazena o resultado para enviar
		String resultado = "";

		try {
			//abrindo o BD
			Class.forName ( "org.sqlite.JDBC" );
			Connection connection = DriverManager.getConnection("jdbc:sqlite:banco.db");
			Statement stmt = connection.createStatement();
			ResultSet rs = null;

			//escolhendo a faunção para realizar a consulta
			switch(Integer.parseInt(input[0])){
				case 1:
					System.out.println("number one");
					rs = stmt.executeQuery( "SELECT * from Perfil where Formacao = '"+ input[1] +"';" );
					break;
				case 2:
					System.out.println("number two");
					rs = stmt.executeQuery( "SELECT Nome,Habilidade from Perfil where Cidade = '"+ input[1] +"';" );
					break;
				case 3:
					System.out.println("number three");
					String ope = "UPDATE Perfil set Habilidade ='"+ input[2] +"' where Email='"+ input[1] +"'";
         	stmt.executeUpdate(ope);
					rs = stmt.executeQuery( "SELECT * from Perfil where Email='"+ input[1] +"';");
					break;
				case 4:
					System.out.println("number four");
					rs = stmt.executeQuery( "SELECT Exp from Perfil where Email = '"+ input[1] +"';" );
					break;
				case 5:
					System.out.println("number five");
					rs = stmt.executeQuery( "SELECT * from Perfil;" );
					break;
				case 6:
					System.out.println("number six");
					rs = stmt.executeQuery( "SELECT * from Perfil where Email = '"+ input[1] +"';" );
					break;
				default:
					System.out.println("Este não é uma ação válida!");
			}

			//imprimindo a saida
			while ( rs.next() ) {
				 if (input[0].equals("2")){
					 String nome  = rs.getString("Nome");
					 String habilidade = rs.getString("Habilidade");

					 System.out.println( "Nome = " + nome );
					 System.out.println( "Habilidade = " + habilidade );
					 System.err.println("-----------------//---------------");

					 name = "Nome = " + nome +  "\n" +
					 				"Habilidade = " + habilidade +  "\n" +
									"-----------------//---------------"+ "\n" ;

				 }
				 else if(input[0].equals("4")){
					 String exp = rs.getString("Exp");

					 System.out.println( "Experiência = " + exp );
					 System.err.println("-----------------//---------------");

					 name = "Experiência = " + exp +  "\n" +
					 				"-----------------//---------------"+ "\n" ;
				 }
				 else{
					 int id = rs.getInt("id");
					 String email = rs.getString("Email");
					 String nome  = rs.getString("Nome");
					 String sobrenome  = rs.getString("Sobrenome");
					 String foto  = rs.getString("Foto");
					 String cidade = rs.getString("Cidade");
					 String formacao = rs.getString("Formacao");
					 String habilidade = rs.getString("Habilidade");
					 String exp = rs.getString("Exp");

					 System.out.println( "ID = " + id );
					 System.out.println( "Email = " + email );
					 System.out.println( "Nome = " + nome );
					 System.out.println( "Sobrenome = " + sobrenome );
					 System.out.println( "foto = " + foto );
					 System.out.println( "cidade = " + cidade );
					 System.out.println( "Formação = " + formacao );
					 System.out.println( "Habilidade = " + habilidade );
					 System.out.println( "Experiência = " + exp );
					 System.err.println("-----------------//---------------");

					 name = "ID = " + id +  "\n" +
								 "Email = " + email  +  "\n" +
								 "Nome = " + nome +  "\n" +
								 "Sobrenome = " + sobrenome +  "\n" +
								 "foto = " + foto +  "\n" +
								 "cidade = " + cidade +  "\n" +
								 "Formação = " + formacao  +  "\n" +
								 "Habilidade = " + habilidade +  "\n" +
								 "Experiência = " + exp +  "\n" +
								 "-----------------//---------------"+ "\n" ;
				 }

				 resultado = name.concat(resultado);

      }
			System.out.println();
			//fechando o BD
      rs.close();
      stmt.close();
      connection.close();

		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}

		/*time 2*/
		long  tv2 = System.nanoTime();
		/*diferença de tempo e printa*/
		long  diff = tv2 - tv1;

		//cria o arquivo de tempos
		try{
			FileWriter myWriter = new FileWriter("tempo_server.txt",true);
			myWriter.append(diff +"\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		//mensagem
		return "Resposta da consulta: " + "\n" + resultado;

	}

	public static void main(String[] args){

		//Apaga o arquivo de tempos
		try{
			PrintWriter writer = new PrintWriter("tempo_server.txt");
			writer.print("");
			writer.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		try {
			//String endereco = "//localhost/MyServer";
			// System.setProperty("java.rmi.server.hostname","192.168.1.83");
			String endereco = "rmi://" + "127.0.0.1" +":"+1099+"/MyServer";
			//endereço de conexão
			// String endereco = "//localhost/MyServer";

			Naming.rebind(endereco, new Server());
			System.err.println("Servidor pronto");

			//Criar BD
			Connection connection = null;

			try {
				Class.forName ( "org.sqlite.JDBC" );
				connection = DriverManager.getConnection("jdbc:sqlite:banco.db");
				System.out.println("Conexão realizada !!!!");
				Statement statement = connection.createStatement();

				//cria banco de dados
				String sql = "DROP TABLE IF EXISTS Perfil;" +
							"CREATE TABLE Perfil(Id INTEGER PRIMARY KEY, Email TEXT, Nome TEXT, Sobrenome TEXT, Foto TEXT, Cidade TEXT, Formacao TEXT, Habilidade TEXT, Exp TEXT);"+
							"INSERT INTO Perfil VALUES (1,'maria_silva@gmail.com','Maria','Silva','C:/home/Pictures/maria.jpg','Campinas','CC','Análise de Dados','Estágio de 1 ano ');"+
							"INSERT INTO Perfil VALUES (2,'msilva@gmail.com','Maria','Silva','C:/home/Pictures/maria.jpg','Campinas','CC','Análise de Dados','Estágio de 1 ano ');"+
							"INSERT INTO Perfil VALUES (3,'maria_silva@gmail.com','Maria2','Silva2','C:/home/Pictures/maria.jpg','Campinas2','CC2','Análise2 de Dados','Estágio de 2 ano' );"+
							"INSERT INTO Perfil VALUES (4,'jsnow@gmail.com','Jon','Snow','C:/home/Pictures/Jon.jpg','Suzano','ENF','Pensamento Lógico','(1)Estágio no HCc');"+
							"INSERT INTO Perfil VALUES (5,'mtilda@gmail.com','Matilda','Bernardes','C:/home/Pictures/matilda.jpg','Americana','FEF','Liderança','(1)Jogou na LNB');"+
							"INSERT INTO Perfil VALUES (6,'bsouza@gmail.com','Bianca','Souza','C:/home/Pictures/Bianca.jpg','São Paulo','MAT','Raciocínio Lógico','(1)Criação de modelos');"+
							"INSERT INTO Perfil VALUES (7,'jteruel@gmail.com','Joana','Teruel','C:/home/Pictures/Joana.jpg','Três Lagoas','MAT','Gerenciamento de Equipes','(1) Scrum');"+
							"INSERT INTO Perfil VALUES (8,'awei@gmail.com','Alex','Wei','C:/home/Pictures/Alex.jpg','Osasco','QUIM','Inovação Tecnológica','(1) Nanotecnologia');"+
							"INSERT INTO Perfil VALUES (9,'rvero@gmail.com','Roberta','Veronez','C:/home/Pictures/Roberta.jpg','Americana','QUIM','Modelador de Soluções','(1)Oganização de Startups');"+
							"INSERT INTO Perfil VALUES (10,'rcruso@gmail.com','Robson','Crusoé','C:/home/Pictures/Robson.jpg','Campinas','EQ','Inovação','(1) Nubank');"+
							"INSERT INTO Perfil VALUES (11,'alesousa@gmail.com','Alessandra','Sousa','C:/home/Pictures/Alessandra.jpg','Campinas','EE','Inovação','(1) Criação de Podcast: SucessoCabeça');"+
							"INSERT INTO Perfil VALUES (12,'aamazzo@gmail.com','Alexa','Amazzo','C:/home/Pictures/Alexa.jpg','Osasco','EC','Mindset criativo','(1)Coach');"+
							"INSERT INTO Perfil VALUES (13,'agoncalves@gmail.com','Amanda','Gonçalves','C:/home/Pictures/Amanda.jpg','Fortaleza','EC','Scrum master','(1) Líder de projeto');"+
							"INSERT INTO Perfil VALUES (14,'nloureda@gmail.com','Natália','Loureda','C:/home/Pictures/Natalia.jpg','Valinhos','AC','Cantar','(1) X Factor');"+
							"INSERT INTO Perfil VALUES (15,'gmendes@gmail.com','Garbriel','Mendes','C:/home/Pictures/Gabriel.jpg','Campinas','AC','Dançar','(1) Circo de Soleil');"+
							"INSERT INTO Perfil VALUES (16,'vsaldanha@gmail.com','Vitor','Saldanha','C:/home/Pictures/Vitor.jpg','Suzano','Pedago','Empatia','(1) EStágio em Escola');"+
							"INSERT INTO Perfil VALUES (17,'pgono@gmail.com',' Pedro','Ono','C:/home/Pictures/Ono.jpg','Fortaleza','BIO','Metódo Cientifico','(1) Análise de Borboleta');"+
							"INSERT INTO Perfil VALUES (18,'bsechin@gmail.com','Beatriz','Sechin','C:/home/Pictures/Beatriz.jpg','São Paulo','BIO','Pensamento Analítico','(1)Criação de borboletas');"+
							"INSERT INTO Perfil VALUES (19,'jmoretoo@gmail.com','Julia','Moretto','C:/home/Pictures/Julia.jpg','Três Lagoas','ENF','Empatia','(1)Estágio no HC');"+
							"INSERT INTO Perfil VALUES (20,'vanzazu@gmail.com','Vanessa','Zazulla','C:/home/Pictures/Vanessa.jpg','Valinhos','FEF','Cordenação de Times','(1)Atleta de alta perfromance');";
				statement.executeUpdate(sql);
				statement.close();
				connection.close();


			} catch (SQLException e) {
			  System.out.println(e.getMessage());
			}

		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}

}
