package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.io.*;  // Import the File class


import javax.swing.JOptionPane;

import rminterface.RMIInterface;

public class Client {

	private static RMIInterface look_up;

	public static void main(String[] args)
		throws MalformedURLException, RemoteException, NotBoundException {
			//endereço de conexão
			//String endereco = "//localhost/MyServer";
			//System.setProperty("java.rmi.client.hostname","177.220.89.110");
			String endereco = "rmi://" + "192.168.1.83" +":"+1099+"/MyServer";
			try {
				look_up = (RMIInterface) Naming.lookup(endereco);
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}

			//Apaga o arquivo de tempos
			try{
				PrintWriter writer = new PrintWriter("tempo_client.txt");
				writer.print("");
				writer.close();
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}

			while(true){
				//recebe dados
				String txt = JOptionPane.showInputDialog("Escolha uma operação");
				/*time 1*/
				long tv1 = System.nanoTime();
				//recebe resposta
				String response = look_up.helloTo(txt);
				System.err.println(response);

				/*time 2*/
				long  tv2 = System.nanoTime();
				/*diferença de tempo e printa*/
				long  diff = tv2 - tv1;

				//cria o arquivo de tempos
				try{
					FileWriter myWriter = new FileWriter("tempo_client.txt",true);
					myWriter.append(diff +"\n");
					myWriter.close();
				} catch (IOException e) {
					System.out.println("An error occurred.");
					e.printStackTrace();
				}
			}
		}
}
